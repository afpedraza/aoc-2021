#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <bitset>

int main()
{
    std::string filename { "input.txt" };
    std::vector<std::string> input {};

    std::ifstream file {filename};
    
    std::string gamma {};
    std::string epsilon {};

    while (file && !file.eof())
    {
        std::string line {};
        file >> line;

        input.push_back(line);
    }

    for (size_t i = 0; i != input[i].size(); ++i)
    {
        size_t count_zero {};
        size_t count_one {};

        for (size_t j = 0; j < input.size(); ++j)
        {
            switch (input[j][i])
            {
                case '0':
                    ++count_zero;
                    break;
                case '1':
                    ++count_one;
                    break;
            }

        }

        std::cout << "zero: " << count_zero << " Ones: " << count_one << "\n";
        
        if (count_zero > count_one)
        {
            gamma.push_back('0');
            epsilon.push_back('1');
        } else {
            gamma.push_back('1');
            epsilon.push_back('0');
        }
        
    }

    
    std::cout << std::stoi(epsilon, 0, 2) * std::stoi(gamma, 0, 2) << std::endl;
    

    return EXIT_SUCCESS;
}