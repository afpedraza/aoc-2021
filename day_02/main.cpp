#include <iostream>
#include <string>
#include <vector>
#include <fstream>

// forward 5 adds 5 to your horizontal position, a total of 5.
// down 5 adds 5 to your depth, resulting in a value of 5.
// forward 8 adds 8 to your horizontal position, a total of 13.
// up 3 decreases your depth by 3, resulting in a value of 2.
// down 8 adds 8 to your depth, resulting in a value of 10.
// forward 2 adds 2 to your horizontal position, a total of 15.

void part1 (std::string& filename);
void part2 (std::string& filename);

int main()
{
    std::string filename { "input.txt" };
    
    part1(filename);
    part2(filename);
}

void part1 (std::string& filename)
{
    std::ifstream file {filename};
    std::string direction {};
    int movement {};

    int horizontal {};
    int depth {};

    while (file && !file.eof())
    {
        file >> direction >> movement;

        if (direction == "up")
        {
            depth -= movement;
        }
        else if (direction == "down"){
            depth += movement;
        }
        else if (direction == "forward")
        {
            horizontal += movement;
        }
        
    }

    std::cout << "Answer part 1 is: " << horizontal * depth << "\n";
}

void part2 (std::string& filename)
{
    std::ifstream file {filename};
    std::string direction {};
    int movement {};
    int aim {};

    int horizontal {};
    int depth {};

    while (file && !file.eof())
    {
        file >> direction >> movement;

        if (direction == "up")
        {
            aim -= movement;
        }
        else if (direction == "down"){
            aim += movement;
        }
        else if (direction == "forward")
        {
            horizontal += movement;
            depth += aim * movement;
        }
        
    }

    std::cout << "Answer part 2 is: " << horizontal * depth << "\n";
}