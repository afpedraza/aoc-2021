#include <iostream>
#include <string>
#include <vector>
#include <fstream>

int part1 (const std::string& file);
int part2 (const std::string& file);

int main()
{
    std::string file_name { "input.txt"};

    //auto answer = part1(file_name);

    std::cout << "Answer part 1: " << part1(file_name) << std::endl;

    //auto answer2 = part2(file_name);

    std::cout << "Answer part 2: " << part2(file_name) << std::endl;
    

}

int part1 (const std::string& file)
{
    std::ifstream input { file };
    std::vector<int> lines {};

    if (!input)
    {
        std::cerr << "File not open\n";
        return 1;
    }

    int n {};
    while (input >> n)
    {
        lines.push_back(n);
    }

    int count {1};

    for (size_t i = 0; i != lines.size(); ++i)
    {
        if (i > 0 && lines[ i+1] > lines[i])
        {
            ++count;
        }
        
    }

    return count;
}

int part2 (const std::string& file){
    std::ifstream input { file };
    std::vector<int> lines {};

    if (!input)
    {
        std::cerr << "File not open\n";
        return 1;
    }

    int n {};
    while (input >> n)
    {
        lines.push_back(n);
    }

    int sum {};
    std::vector<int> sum_windows {};

    for (size_t i = 0; i != lines.size(); ++i)
    {
        if (i >= 2 )
        {
            sum = lines[i] + lines[ i - 1] + lines[i - 2];
            sum_windows.push_back(sum);
        }
        
    }

    int count {1};

    for (size_t i = 0; i != sum_windows.size(); ++i)
    {
        if (i > 0 && sum_windows[ i+1] > sum_windows[i])
        {
            ++count;
        }
        
    }

    return count;

}
